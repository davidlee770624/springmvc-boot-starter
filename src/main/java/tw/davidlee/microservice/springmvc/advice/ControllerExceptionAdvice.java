package tw.davidlee.microservice.springmvc.advice;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import tw.davidlee.microservice.springmvc.enums.ApiEnum;
import tw.davidlee.microservice.springmvc.exception.ServiceException;
import tw.davidlee.microservice.springmvc.http.ApiResponse;

import javax.servlet.http.HttpServletRequest;

/**
 *    所有webmvc模組 可引用相同的錯誤回應
 */
@Slf4j
@RestControllerAdvice
public class ControllerExceptionAdvice {

    /**
    *  Controller 拋出HttpRequestMethodNotSupportedException之處理
    * */
    @ExceptionHandler(value = HttpRequestMethodNotSupportedException.class)
    @ResponseStatus(code = HttpStatus.METHOD_NOT_ALLOWED)
    public ApiResponse handleMethodNotSupportedException(HttpServletRequest req , HttpRequestMethodNotSupportedException e) {
        String message = e.getMessage();
        log.warn( "URI={} , Message={}", req.getRequestURI() , message );
        return new ApiResponse(ApiEnum.REQUEST_EXCEPTION,message);
    }

    /**
     *  Controller 拋出ServiceException之處理
     * */
    @ExceptionHandler(value = ServiceException.class)
    @ResponseStatus(code = HttpStatus.OK)
    public ApiResponse handleServiceException(HttpServletRequest req , ServiceException e) {
        String message = e.getMessage();
        log.warn( "URI={} , Message={}", req.getRequestURI() , message );
        return new ApiResponse(ApiEnum.SERVICE_EXCEPTION,message);
    }

    /**
     *  Controller 拋出Exception之處理
     * */
    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
    public ApiResponse handleException(HttpServletRequest req , Exception e) {
        String message = e.getMessage();
        log.error( "URI={} , Message={}", req.getRequestURI() , message );
        return new ApiResponse(ApiEnum.EXCEPTION);
    }

}
