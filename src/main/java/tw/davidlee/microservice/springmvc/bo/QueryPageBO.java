package tw.davidlee.microservice.springmvc.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 查詢頁面條件
 * */
@Data
@ApiModel(description = "查詢頁面條件")
public class QueryPageBO {

    /**
     * 當前頁面位置
     * */
    @ApiModelProperty(value = "顯示頁面位置,1開始", required = true , example = "1")
    private long current;

    /**
     * 當前頁面筆數
     * */
    @ApiModelProperty(value = "每頁頁面筆數", required = true , example = "10")
    private long size;

}
