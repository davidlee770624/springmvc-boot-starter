package tw.davidlee.microservice.springmvc.enums;

import lombok.Getter;

/**
 * 所有webmvc模組 可引用相同的業務處理結果
 * */
public enum ApiEnum {

    OK (  001 , "成功!"),
    EXCEPTION (  002 , "發生未預期錯誤!"),
    REQUEST_EXCEPTION (  003 , "請求發生錯誤!"),
    SERVICE_EXCEPTION (  004 , "業務處理發生錯誤!");

    @Getter
    private Integer code;

    @Getter
    private String msg;

    ApiEnum(int code , String msg ) {
        this.code = code;
        this.msg = msg;
    }
}
