package tw.davidlee.microservice.springmvc.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
@ComponentScan(value="tw.davidlee.microservice.springmvc.advice")
public class WebMvcConfig {


}
