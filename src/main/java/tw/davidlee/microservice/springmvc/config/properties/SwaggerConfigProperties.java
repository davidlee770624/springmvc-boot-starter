package tw.davidlee.microservice.springmvc.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "tw.davidlee.swager" )
public class SwaggerConfigProperties {
    /**
     * Controller Package路徑
     * */
    private String basePackage;
    /**
     * 服務抬頭
     * */
    private String title;
    /**
     * 服務說明
     * */
    private String description;
}
