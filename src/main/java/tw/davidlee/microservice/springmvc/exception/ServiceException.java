package tw.davidlee.microservice.springmvc.exception;

import lombok.Data;

/**
 * 所有Service業務拋出錯誤
 * */
@Data
public class ServiceException extends RuntimeException{
    private String message = "";
    public ServiceException(String message){
        this.message = message;
    }
}
