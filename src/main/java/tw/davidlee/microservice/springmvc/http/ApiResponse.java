package tw.davidlee.microservice.springmvc.http;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import tw.davidlee.microservice.springmvc.enums.ApiEnum;
import lombok.Getter;

/**
 * 所有webmvc模組 可引用相同的Http Response結果
 * */
@ApiModel(description = "API回應內容")
public class ApiResponse<T> {
    @Getter
    @ApiModelProperty(value = "處理結果代號", required = true)
    private Integer systemCode;
    @Getter
    @ApiModelProperty(value = "處理結果說明", required = true)
    private String systemMsg;
    @Getter
    @ApiModelProperty(value = "回應資料")
    private T data;

    public ApiResponse() {
        this.systemCode = ApiEnum.OK.getCode();
        this.systemMsg = ApiEnum.OK.getMsg();
    }

    public ApiResponse(ApiEnum apiEnum) {
        this.systemCode = apiEnum.getCode();
        this.systemMsg = apiEnum.getMsg();
        this.data = (T) "";
    }

    public ApiResponse(T data) {
        this.systemCode = ApiEnum.OK.getCode();
        this.systemMsg = ApiEnum.OK.getMsg();
        this.data = data;
    }

    public ApiResponse(ApiEnum apiEnum, T data) {
        this.systemCode = apiEnum.getCode();
        this.systemMsg = apiEnum.getMsg();
        this.data = data;
    }

}
